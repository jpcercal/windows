@ECHO off

:: Cekurte Sistemas

:: Especifica um número de página de código
:: UTF-8 Unicode
CHCP 65001 

:: Define as variáveis utilizadas pelo script
SET APP_TITLE=Laboratório
SET APP_VERSION=1.0

SET USER_FOLDER=%LOGONSERVER%\Shared\%USERNAME%

SET LINE=--------------------------------------------------------------------------------

:: Apresenta um titulo para o aplicativo
TITLE %APP_TITLE% v%APP_VERSION%
CLS

:: #############################################################################################################

:: Escrever para o usuário na tela inicial: Olá (login do usuário), bem vindo ao domínio (nome do domínio),
:: você está conectado ao host (nome do computador)
ECHO %LINE%
ECHO %APP_TITLE%
ECHO.
ECHO Olá %USERNAME%, bem vindo ao domínio %USERDOMAIN%.
ECHO você está conectado ao host %COMPUTERNAME%!
ECHO.
TITLE %APP_TITLE% v%APP_VERSION% - 10%% concluído

:: #############################################################################################################

:: Remover todos os mapeamentos existentes
ECHO %LINE%
ECHO Removendo todos os mapeamentos existentes [...]
ECHO.
NET USE * /delete /yes
TITLE %APP_TITLE% v%APP_VERSION% - 20%% concluído


:: #############################################################################################################

:: Mapear a unidade de rede X, onde o nome da pasta mapeada é o login do usuário logado (Se for possível, 
:: use o artificio de criar a pasta do usuário, caso ela não exista no servidor
ECHO %LINE%
ECHO Verificando se a pasta para o usuário %USERNAME% já existe [...]
ECHO.

IF exist %USER_FOLDER% ( ECHO A pasta para o usuário %USERNAME% já existe! ) ELSE ( MKDIR %USER_FOLDER% && ECHO Diretório %USER_FOLDER% criado com sucesso!)

ECHO.
TITLE %APP_TITLE% v%APP_VERSION% - 30%% concluído

:: #############################################################################################################

:: Atribui as permissões a pasta
ECHO %LINE%
ECHO Atribuindo permissões a pasta do usuário [...]
ECHO.
ECHO S| CACLS %USER_FOLDER% /T /G %USERDOMAIN%\%USERNAME%:F
ECHO.
TITLE %APP_TITLE% v%APP_VERSION% - 40%% concluído

:: #############################################################################################################

:: Mapeia a unidade de rede
ECHO %LINE%
ECHO Mapeando as unidades de rede [...]
ECHO.
NET USE x: %USER_FOLDER%

:: Mapear o compartilhamento de uma pasta chamado DADOS onde todos os usuários do domínio terão acesso.
NET USE z: %LOGONSERVER%\dados

TITLE %APP_TITLE% v%APP_VERSION% - 50%% concluído

:: #############################################################################################################

:: Limpar conteúdo da pasta C:\WINDOWS\Temp
ECHO %LINE%
ECHO Limpando o conteudo do diretório %WINDIR%\temp [...]
ECHO.
del %WINDIR%\temp\ /f /q /s
ECHO.
TITLE %APP_TITLE% v%APP_VERSION% - 60%% concluído

:: #############################################################################################################

:: Mapear uma impressora virtual, pdf creator por exemplo, e torna-la padrão para o usuário logado (o
:: instalador você deve conseguir)

:: Fazer com que seu script chame outro script para instalar um programa que compacte e descompacte
:: arquivos (a decisão de qual programa utilizar, bem como o instalador é por sua conta)

:: Fazer seu script de logon mudar o papel de parede para uma imagem da sua escolha.

:: #############################################################################################################

:: FIM
ECHO %LINE%
ECHO Processo concluído com sucesso!
ECHO.
TITLE %APP_TITLE% v%APP_VERSION% - 100%% concluído

PAUSE